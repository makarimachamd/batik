<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="ass/css/style_header.css">
</head>
<body>
	<!-- MAIN HEADER -->
	<div id="header">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- LOGO -->
				<div class="col-md-3">
					<div class="header-logo">
						<a href="#" class="logo">
							<img src="" alt="">
						</a>
					</div>
				</div>
				<!-- /LOGO -->

				<!-- SEARCH BAR -->
				<div class="col-md-6">
					<div class="header-search">
						<!-- <input class="input" placeholder="Search here"> -->
						<form class="mx-3 my-auto d-inline w-100">
							<div class="input-group" style="border-color: #fff; border: 5;">
								<input type="text" class="form-control border border-right-0" placeholder="Search...">
								<span class="input-group-append">
									<button class="btn btn-outline-secondary border border-left-0" type="button" style="background-color: #D69962">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</form>
						
					</div>
					<div class="menu purple pullUp">
						<a>HOME</a>
						<a>ARTICLES</a>
						<a>PORTFOLIO</a>
						<a>ABOUT</a>
						<a>CONTACT</a>
					</div>
				</div>
				<!-- /SEARCH BAR -->
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>
	<!-- /MAIN HEADER -->
</body>
</html>