<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="ass/js/jscountdown.js"></script>
	<link rel="stylesheet" type="text/css" href="ass/css/style_countdown.css">
</head>
<body>
	<div class="container">
		<section id="timer">
			<div class="row">
				<div class="countdown-wrapper">
					<div class="card">
						<div class="card-header">
							Upcomming Event countdown timer
						</div>
						<div class="card-block">
							<div id="countdown">
								<div class="row text-center well justify-content-center">
									<div id="hour" class="col-3 timer"></div><p style="font-size: 36px;">:</p>
									<div id="min" class="col-3 timer"></div><p style="font-size: 36px;">:</p>
									<div id="sec" class="col-3 timer"></div>
								</div>
								<div class="row text-center well justify-content-center">
									<div class="col-3">
										Jam
									</div>
									<div class="col-3">
										Menit
									</div>
									<div class="col-3">
										Detik
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<a href="#" class="btn btn-primary">Book now</a>
							<a href="#" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Subscrib for upcomming</a>
							<a href="#" class="btn btn-danger">14 remaining</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<!-- Subscribe for upcomming event modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Subscribe for our upcomming Events</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="email">Email address</label>
							<input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email"> 
							<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> 
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success">Save changes</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>