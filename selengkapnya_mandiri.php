<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="UTF-8">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="Boost/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="ass/css/style_tambahan.css">
	<link rel="stylesheet" href="css/style_regis.css" >
	<script src="Boost/js/jquery.min.js"></script>
	<script type="text/javascript" src="Boost/js/bootstrap.min.js"></script>

	<style>
	body {
		font-family: "Lato", sans-serif;
	}
</style>


</head>
<body>

	<?php
	include 'navbar.php';
	include 'header.php';
	?>
	<div class="row">
		<div class="col"></div>
		<div class="col-8 outter-form" style="padding: 1%;">
			<div class="row">
				<div class="col"></div>
				<div class="col-6 outter-form">
					<div class="row" style="border: 1px solid #bec2c6;">
						<div class="col align-self-center">ATM MANDIRI</div>
						<div class="col align-self-center">
							<label for="check">Click me</label>
						</div>
					</div>
					<input id="check" type="checkbox">

					<div class="test">
						<table>
							<tr>
								<td style="color: #a0a5af; font-size: 12px; font-weight: bold;">
									1. Masukkan kartu ATM<br>
									2. Pilih menu "Bayar/Beli"<br>
									3. Pilih menu "Lainnya" hingga menemukan menu "Multiplayment"<br>
									4. Masukkan Kode Biller Tokopedia (88708), lalu pilih benar<br>
									5. Masukkan "nomor virtual akun" Tokopedia, lalu pilih tombol benar<br>
									6. Masukkan angka "1" untuk memilih tagihan, lalu pilih tombol Ya<br>
									7. Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya<br>
									8. Simpan struk sebagai bukti pembayaran Anda
								</td>
							</tr>
						</table>
					</div>
					<center><a href="#" style="color: #4ebf74; font-size: 12px; font-weight: bold;">Detail Pembayaran</a></center>
					<button class="btn btn-block btn-custom-green" style="width: 100%; margin-top: 2%; font-size: 100%;" type="submit">Cek Status Pembayaran</button><br>
					<img style="float: left; width: 20%" src="Gambar/visa720.png">
					<img style="float: left; width: 20%; margin-left: 6%;" src="Gambar/jne360.png">
					<img style="float: right; width: 20%;" src="Gambar/tiki240.png">
					<img style="float: right; width: 20%;  margin-right: 6%;" src="Gambar/visa720.png">
				</div>
				<div class="col"></div>
			</div>
		</div>
		<div class="col"></div>
		
	</div>
</body>
</html>