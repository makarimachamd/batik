<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="Boost/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="ass/css/style_tambahan.css">
	<link rel="stylesheet" type="text/css" href="ass/css/style_countdown.css">
	<script src="Boost/js/jquery.min.js"></script>
	<script type="text/javascript" src="Boost/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="ass/js/jscountdown.js"></script>

	<style>
	body {
		font-family: "Lato", sans-serif;
	}
</style>
<link rel="stylesheet" type="text/css" href="Boost/css/bootstrap.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="ass/css/style_tambahan.css" >

</head>
<body>

	<?php
	include 'navbar.php';
	include 'header.php';
	?>
	<div class="row justify-content-center">
		<div class="col-8 outter-form" style="padding: 1%">
			<div class="row justify-content-center">
				<div class="col-6">
					<p style="color: #9a9fa8; font-size: 14px; font-weight: bold;">Segera Selesaikan Pembayaran Anda Sebelum Stok Habis</p>
					<div class="container">
						<section id="timer">
							<div class="row">
								<div class="countdown-wrapper">
									<div class="card-block" style="background: lightgrey; border-radius: 3px;">
										<div id="countdown" style="padding: 14%;">
											<div class="row text-center justify-content-center">
												<div id="hour" class="col-3 timer"></div><p style="font-size: 36px;">:</p>
												<div id="min" class="col-3 timer"></div><p style="font-size: 36px;">:</p>
												<div id="sec" class="col-3 timer"></div>
											</div>
											<div class="row text-center well justify-content-center">
												<div class="col-3">
													Jam
												</div>
												<div class="col-3">
													Menit
												</div>
												<div class="col-3">
													Detik
												</div>
											</div>
											<div class="row" style="margin-top: 3%;">
												<div class="col text-center">
													<p style="font-style: italic; color: grey; font-size: 12px;">Sebelum Senin 24 Desember 2018 pukul 03:54 WIB</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
					<br>
					<table style="background: #fef7da; width: 100%; text-align: center;">
						<tr>
							<td style="color: #b7b4ae; font-size: 12px; padding: 2%; font-weight: bold;">
								Pastikan transaksi mandiri virtual account anda telah terverifikasi sebelum melakukan transaksi kembali dengan metode yang sama
							</td>
						</tr>
					</table>
					<p style="color: #b7b4ae; font-size: 12px; padding: 2%; font-weight: bold; margin-bottom: 0%;">Transfer pembayaran ke nomor virtual account</p>
					<img style="float: left; margin-bottom: 1%;" width="20%" src="Gambar/visa.png">
					<p style="color: #b7b4ae;float: right; margin-right: 25%; font-size: 18px; padding: 2%; font-weight: bold; padding-top: 0%;">0999658498548758478
					</p><br><br>
					<a href="#" style="color: #4ebf74; font-size: 12px; font-weight: bold; margin-right: 0px; padding-top: 0%;">Salin No. Rek</a>
					<hr>
					<p style="color: #b7b4ae; font-size: 12px; font-weight: bold;">Jumlah yang harus dibayar :</p>
					<div class="col-md-auto" style="color: #d69962; font-size: 14px; font-weight: bold;">Rp. 14.750.098
					</div>
					<a href="#" style="color: #4ebf74; font-size: 12px; font-weight: bold; margin-right: 0px; padding-top: 0%;">Salin No. Rek</a><br><br>
					<!---->
					<div class="container" style="border: 1px solid #bec2c6;">
						<div class="row" style="border: 1px solid #bec2c6;">
							<div class="col-10 align-self-center">ATM MANDIRI</div>
							<div class="col-2 align-self-center">
								<label for="check">V</label>
							</div>
						</div>
						<input id="check" type="checkbox">

						<div class="test">
							<table>
								<tr>
									<td style="color: #a0a5af; font-size: 12px; font-weight: bold;">
										1. Masukkan kartu ATM<br>
										2. Pilih menu "Bayar/Beli"<br>
										3. Pilih menu "Lainnya" hingga menemukan menu "Multiplayment"<br>
										4. Masukkan Kode Biller Tokopedia (88708), lalu pilih benar<br>
										5. Masukkan "nomor virtual akun" Tokopedia, lalu pilih tombol benar<br>
										6. Masukkan angka "1" untuk memilih tagihan, lalu pilih tombol Ya<br>
										7. Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya<br>
										8. Simpan struk sebagai bukti pembayaran Anda
									</td>
								</tr>
							</table>
						</div>
						<div class="row" style="border: 1px solid #bec2c6;">
							<div class="col-10 align-self-center">Mandiri Internet Banking / Mandiri Online</div>
							<div class="col-2">
								<label for="e-banking">V</label>
							</div>
						</div>
						<input id="e-banking" type="checkbox">

						<div class="test">
							<table>
								<tr>
									<td style="color: #a0a5af; font-size: 12px; font-weight: bold;">
										1. Masukkan kartu ATM<br>
										2. Pilih menu "Bayar/Beli"<br>
										3. Pilih menu "Lainnya" hingga menemukan menu "Multiplayment"<br>
										4. Masukkan Kode Biller Tokopedia (88708), lalu pilih benar<br>
										5. Masukkan "nomor virtual akun" Tokopedia, lalu pilih tombol benar<br>
										6. Masukkan angka "1" untuk memilih tagihan, lalu pilih tombol Ya<br>
										7. Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya<br>
										8. Simpan struk sebagai bukti pembayaran Anda
									</td>
								</tr>
								<tr>
									<td style="color: #a0a5af; font-size: 12px; font-weight: bold;">
										Jangan gunakan fitur "simpan daftar transfer" untuk pembayaran melalui internet Banking karena dapat mengganggu proses pembayaran berikutnya<br><br>
										Untuk menghapus daftar transfer tersimpan ikuti langkah berikut :<br>
										1. Login Mandiri Online<br>
										2. Pilih ke menu "Pembayaran"<br>
										3. Pilih menu daftar pembayaran<br>
										4. Pilih pada pembayaran yang tersimpan, lalu pilih menu untuk hapus
									</td>
								</tr>
							</table>
						</div>
					</div>
					
					<center><a href="#" style="color: #4ebf74; font-size: 12px; font-weight: bold;">Detail Pembayaran</a></center>
					<button class="btn btn-custom-coklat" style="width: 100%; margin-top: 2%; margin-bottom: 2%; font-size: 100%;" type="submit">Cek Status Pembayaran</button><br>
					<img style="float: left; width: 20%" src="Gambar/visa720.png">
					<img style="float: left; width: 20%; margin-left: 6%;" src="Gambar/jne360.png">
					<img style="float: right; width: 20%;" src="Gambar/tiki240.png">
					<img style="float: right; width: 20%;  margin-right: 6%;" src="Gambar/visa720.png">
				</div>
			</div>
		</div>
	</div>
</div>




</body>
</html>