<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="ass/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" type="text/css" href="Boost/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="ass/css/style_tambahan.css">
	<script src="Boost/js/jquery.min.js"></script>
	<script type="text/javascript" src="Boost/js/bootstrap.min.js"></script>
</head>
<body>
	<form action="" method="post">
		<div class="row" style="margin-top: 4%;">
			<div class="col-1"></div>
			<!--Input-->
			<div class="col-sm-6 outter-form">
				<h4 style="margin-top: 2%;">Alamat Pengiriman</h4>
				<hr>
				<div class="row">
					<div class="col">
						<h5>Nama Penerima</h5>
						<input type="text" class="form-control input-lg" id="inputlg" name="username" placeholder="mohon isikan nama lengkap">
						<h5>Nomor HP</h5>
						<input type="text" class="form-control input-lg" id="inputlg" name="username" placeholder="masukkan nomor hp aktif">
						<h5>Label Alamat</h5>
						<input type="text" class="form-control input-lg" id="inputlg" name="username" placeholder="tulis nama alamat">
					</div>
					<div class="col">
						<h5>Kecamatan atau Kota</h5>
						<input type="text" class="form-control input-lg" id="inputlg" name="username" placeholder="Isi nama lengkap anda">
						<h5>Alamat</h5>
						<input type="text" class="form-control input-lg" id="inputlg" name="username" placeholder="tuliskan nama jalan, nomor rumah, nama gedung">
						<h5>Kode POS</h5>
						<input type="text" class="form-control input-lg" id="inputlg" name="username" placeholder="Isi nama lengkap anda">
					</div>
				</div>
			</div>
			<div style="margin: 1%;"></div>
			<!--Hasil-->
			<div class="col-sm-4 outter-form">
				<div class="row" style="margin-top: 12%; margin-left: 5%;">
					<div>
						<h3>Sub Total</h3>
					</div>
					<div class="col">
						(3barang)
					</div>
					<div>Rp</div>
					<div class="col">
						<h3 style="color: #d69962;">360,000</h3>
					</div>
				</div>
				<div class="row" style="margin-left: 5%;">
					<div>
						<h5>Biaya Pengiriman</h5>
					</div>
					<div class="col">
					</div>
					<div>Rp</div>
					<div class="col">
						<h3 style="color: #d69962;">8,000</h3>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col"><hr></div>
				</div>

				<div class="row" style="margin-left: 5%;">

					<div>
						<h2 style="color: lightblue;">TOTAL</h2>
					</div>
					<div class="col">
					</div>
					<div>Rp</div>
					<div class="col">
						<h3 style="color: #d69962;">368,000</h3>
					</div>
				</div>
				
				<div class="row " style="margin-top: 3%">
					<a href="#" style="float: left; height: 50px; width: 200px;" id='modal-launcher' class="a" data-toggle="modal" data-target="#login-modal">
						<input type="submit" class="btn btn-block btn-custom-coklat tombolregis" value="Buat pesanan sekarang" />
					</a>
				</div>
				
			</div>
		</div>
	</form>
	<!--Konfirmasi Alamat-->
	<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 5%;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header login_modal_header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						x
					</button>
				</div>
				<div class="modal-body login-modal">
					<div class="clearfix"></div>
					<div id='social-icons-conatainer'>
						<div class='modal-body-left'>
							<h4 class="modal-title" id="myModalLabel">Konfirmasi Alamat Pengiriman</h4><br><br>
							<div>
								<p style="margin: 0px;color: grey; font-size: 12px;">namakuabcdefghijk</p>
								<p style="margin: 0px;color: grey; font-size: 12px;">081234567890</p>
								<p style="margin: 0px;color: grey; font-size: 12px;">rumah</p>
								<p style="margin: 0px;color: grey; font-size: 12px;">kota jogja</p>
								<p style="margin: 0px;color: grey; font-size: 12px;">jl. abcdefghijkl</p>
								<p style="margin: 0px;color: grey; font-size: 12px;">1234</p>
							</div>
							
							<a href="#" style="float: right; height: 50px; margin-bottom: 0px;" id='modal-launcher' class="a" data-toggle="modal" data-target="#bank-modal">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #d69962">Konfirmasi</button>
							</a>
						</div>
					</div>   
				</div>                                                                                                                
				<div class="clearfix"></div>
			</div>
			
		</div>
	</div>
	<!--Pilih bank-->
	<div class="modal fade" id="bank-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 5%;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header login_modal_header">
					<div class="col">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							x
						</button>
					</div>
				</div>
				<div class="modal-body login-modal">
					<div id='conatainer'>
						<div class='modal-body-left'>
							<div class="container">
								<div class="row" style="border-radius: 5px; border: 1px solid #bec2c6;">
									<div class="align-self-center"style="margin-left: 2%;">
										<img src="gambar/fb128.png">
									</div>
									<div class="col align-self-center">
										<h5>BRI</h5>
										<p>Akun virtual BRI</p>
									</div>
									<div class="col align-self-center"style="float: right; margin-right: 2%;">
										<a href="#" style="color: grey;" id='modal-launcher' class="a" data-toggle="modal" data-target="#pembayaran-modal">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: grey">></button>
										</a>
									</div>
								</div>
							</div>
							
						</div>
					</div>   
				</div>                          
			</div>
		</div>
	</div>
	<!--Detail Tagihan-->
	<div class="modal fade" id="tagihan-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 5%;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header login_modal_header" style="margin-bottom: 0px;">
					<div>
						<a href="#" style="float: left;" id='modal-launcher myModalLabel' class="a modal-title" data-toggle="modal" data-target="#bank-modal">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: grey;"><</button>
						</a>
					</div>
					<div class="col-8">
						<h4 class="modal-title" id="myModalLabel">Detail Tagihan</h4>
					</div>
					<div class="col">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							x
						</button>
					</div>
				</div>
				<div class="container row" style="color: grey; margin-top: 5%;">
					<div class="col-7">
						<p style="margin-bottom: 0px;">Nama Barang</p>
						<p>1x Rp360.000</p>
					</div>
					<div class="col">
						<p>Rp</p>
					</div>
					<div class="col">
						<p>360.000</p>
					</div>
				</div>
				<div class="container row" style="color: grey">
					<div class="col-7">
						<p style="margin-bottom: 0px;">Biaya kirim yang dibebankan</p>
						<p>1x Rp360.000</p>
					</div>
					<div class="col">
						<p>Rp</p>
					</div>
					<div class="col">
						<p>8.000</p>
					</div>
				</div>
				<hr>
				<div class="container row" style="color: grey">
					<div class="col-7">
						<h4>Tagihan</h4>
					</div>
					<div class="col">
						<p>Rp</p>
					</div>
					<div class="col">
						<p>368.000</p>
					</div>
				</div>

				<a href="#" style="float: right; height: 50px; margin-bottom: 0px; margin-right: 2%;" id='modal-launcher' class="a" data-toggle="modal" data-target="#bank-modal">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #d69962">OK</button>
				</a>         
			</div>
		</div>
	</div>
	<!--pembayaran modal-->
	<div class="modal fade" id="pembayaran-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 5%;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header login_modal_header" style="margin-bottom: 0px;">
					<div>
						<a href="#" style="float: left;" id='modal-launcher myModalLabel' class="a modal-title" data-toggle="modal" data-target="#bank-modal">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: grey;"><</button>
						</a>
					</div>
					<div class="col-8">
						<h4 class="modal-title" id="myModalLabel">Pembayaran</h4>
					</div>
					<div class="col">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							x
						</button>
					</div>
				</div>

				<div class="container" style="margin-top: 5%;">
					<div class="row" style="border: 1px solid #bec2c6; padding: 3%; border-radius: 3px; margin-right: 1%; margin-left: 1%;">
						<div class="col align-self-center">
							<h3>RP 368.000</h3>
						</div>
						<div class="col align-self-center">
							<a href="#" style="color: #d69962; float: right;">Detail Tagihan</a>
						</div>
					</div>
					<div class="row" style="border: 1px solid #bec2c6; padding: 1%; border-radius: 3px; margin-right: 1%; margin-left: 1%; color: #b7b4ae;">
						<div class="col-7 align-self-center">
							<p>Mandiri virtual akun</p>
						</div>
						<div class="col align-self-center">
							<img src="gambar/visa240.png">
						</div>
					</div>
					<div class="row" style="border: 1px solid #bec2c6; padding: 1%; border-radius: 3px; margin-right: 1%; margin-left: 1%;">
						<div class="col align-self-center" style="background: #fef7da; border: 1px solid #d69962; border-radius: 3px; margin-right: 1%; margin-left: 1%; color: #b7b4ae; font-size: 14px; padding: 2%; font-weight: bold;">
							<p>
								Pastikan transaksi mandiri virtual account anda telah terverifikasi sebelum melakukan transaksi kembali dengan metode yang sama
							</p>
						</div>
					</div>
					<div class="row" style="padding: 1%; margin-right: 1%; margin-left: 1%;">
						<div class="col">
							<table>
								<tr>
									<td>
										<p>Periksa kembali data pembayaran anda sebelum emlanjutkan transaksi</p>
									</td>
								</tr>
								<tr>
									<td>
										<p>Gunakan kode pembayaran Mandiri Virtual Account untuk membayar transaksi ini</p>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<input type="submit" class="btn btn-custom-coklat" value="Bayar" />
						</div>
					</div>
				</div>		
				<a href="#" style="float: right; height: 50px; margin-bottom: 0px; margin-right: 2%;" id='modal-launcher' class="a" data-toggle="modal" data-target="#bank-modal">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #d69962">OK</button>
				</a>         
			</div>
		</div>
	</div>
</body>
</html>