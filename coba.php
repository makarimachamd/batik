<!DOCTYPE html>
<html>
<head>
	<title>tes</title>
	<style type="text/css">
	.purple
	{
		background: #673ab7;
	}
	div.menu
	{
		font-family: Raleway;
		margin: 0 auto;
		padding: 10em 3em;
		text-align: center;
	}

	div.menu a
	{
		color: #FFF;
		text-decoration: none;
		font: 20px Raleway;
		margin: 0px 10px;
		padding: 10px 10px;
		position: relative;
		z-index: 0;
		cursor: pointer;
	}
	/*topbottomborderin*/
	div.pullUp a:before
	{
		position: absolute;
		width: 100%;
		height: 2px;
		left: 0px;
		bottom: 0px;
		content: '';
		background: #FFF;
		opacity: 0.3;
		transition: all 0.3s;
	}

	div.pullUp a:hover:before
	{
		height: 100%;
	}
</style>
</head>
<body>
	<div class="menu purple pullUp">
		<a>HOME</a>
		<a>ARTICLES</a>
		<a>PORTFOLIO</a>
		<a>ABOUT</a>
		<a>CONTACT</a>
	</div>
</body>
</html>