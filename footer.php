<!DOCTYPE html>
<html>
<head>
	<title>Batik Bayat</title>
	<meta name="viewport" content="width=device-width, initialscale=1">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="boost/css/bootstrap.min.css"/>
	<!-- Font -->
	<link rel="stylesheet" href="ass/css/font-awesome.min.css">
</head>
<body style="background-color: #eff0f5">
	<!-- Footer -->
	<footer id="footer">
			<!-- top footer -->
			<div class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!--tentang batik bayat-->
						<div class="col-md-3">
							<div class="footer">
								<h3>Tentang BatikBayat</h3>
								<p>Industri batik asal daerah Bayat, Kabupaten Klaten yang sudah berdiri sejak Januari 2010. Memperdayakan masyarakat sekitar untuk meningkatkan perekonomian warga Bayat.</p>
							</div>
						</div>
						<!-- via pembayaran dan jasa kirim-->
						<div class="col-md-6 col-xs-6">
							<div class="footer">
								<ul class="footer-payments">
									<li><a href="#"><img src="gambar/visa240.png" ></a></li>
									<li><a href="#"><img src="gambar/mastercard240.png"></a></li>
									<li><a href="#"><img src="gambar/jne240.png"></a></li>
									<li><a href="#"><img src="gambar/tiki240.png"></a></li>
								</ul>
							</div>
						</div>

						<!--Kontak-->
						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3>Kontak Kami</h3>
								<ul class="footer-links">
									<li><a href="#"><img src="gambar/fb128.png"> batikbayat</a></li>
									<li><a href="#"><img src="gambar/wa128.png"> 08123456789</a></li>
									<li><a href="#"><img src="gambar/twitter128.png"> @batikbayat</a></li>
									<li><a href="#"><img src="gambar/ig128.png"> @batikbayat</a></li>
									<li><a href="#"><img src="gambar/email128.png"> batikbayat@gmail.com</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /top footer -->

			<!-- bottom footer -->
			<div id="bottom-footer" class="section">
				<div class="container">
					<!-- row -->
					<h3>Lokasi Butik</h3>
					<a href="#"><img src="gambar/pin64.png"></a>
					<div class="row">
						<div class="col-md-12 text-center">
							<span class="copyright">
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
								Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | BatikBayat oleh Teknik Informatika UPNVYK
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							</span>
						</div>
					</div>
						<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /bottom footer -->
		</footer>
		<!-- /FOOTER -->
	</body>
</html>