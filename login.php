<!DOCTYPE html>
<html lang="en">
<head>
	<title>Halaman Login</title>

	<link href="ass/css/bootstrap.min.css" rel="stylesheet">
	<link href="ass/css/style_tambahan.css" rel="stylesheet">

</head>
<body>
	<div class="col-md-4 col-md-offset-4 form-login">


		<h2 class="text-center title-login" style="font-family: : times new roman; font-weight: bold;">BATIKBAYAT
		</h2><br>
		<div class="outter-form-login">
        	<!-- <div class="logo-login">
            	 	<em class="glyphicon glyphicon-user"></em>
        		 </div> -->
        	<form action="check-login.php" class="inner-login" method="post">
        		<h3 class="text-center title-login" style="font-weight: bold;">MASUK?</h3>
        		<h5 class="text-center title-login">
        			Anggota baru? <a href="#" class="a">Daftar Disini</a>
        		</h5><br>
        		<h4>No. Telp atau Email</h4>
        		<div class="form-group">
        			<input type="text" class="form-control input-lg" id="inputlg" name="username" placeholder="Username">
        		</div><br>
        		<h4>Kata Sandi</h4>
        		<div class="form-group">
        			<input type="password" class="form-control input-lg" id="inputlg" name="password" placeholder="Password">
        		</div>
        		<input type="checkbox" name="">
        			Ingat Saya<a href="#" style="float: right;" id='modal-launcher' class="a" data-toggle="modal" data-target="#login-modal">Lupa Kata Sandi?</a>
        			<br><br>
        		<input type="submit" class="btn btn-block btn-custom-coklat" value="Masuk" /><br>
        	</form>
    	</div>
	</div>

	<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header login_modal_header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Kirim Ke Alamat Email Anda?</h4>
				</div>
				<div class="modal-body login-modal">
					<div class="clearfix"></div>
					<div id='social-icons-conatainer'>
						<div class='modal-body-left'>
							<form class="form-signin" role="form" method="POST" action="valid-user.php">
								<div class="form-group">
									<input type="text" name="users_username" class="form-control" placeholder="Masukkan alamat email anda" required autofocus>
									<i class="fa fa-user login-field-icon"></i>
								</div>
								<button class="btn btn-block btn-custom-green" style="float: right; width: 100px;" type="submit">Kirim
								</button>
								<br><br/>
							</form>
						</div>
					</div>   
				</div>                                                                                                                
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<div class="modal-footer login_modal_footer"></div>
	</div>
</div>

<script type="text/javascript" src="dist/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="dist/js/bootstrap.min.js"></script>   

</body>
</html>