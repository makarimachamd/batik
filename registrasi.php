<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="ass/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" type="text/css" href="Boost/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="ass/css/style_tambahan.css">
	<script src="Boost/js/jquery.min.js"></script>
	<script type="text/javascript" src="Boost/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="col-md-6 col-md-offset-6 form-regis" >
		
		<div class="outter-form-regis">
			<form action="check-registrasi.php" class="inner-regis" method="post"><br>
				<h2 class="title-regis">Daftar Pengguna Baru</h2><br>
				<h6>Nama Lengkap</h6>
				<div class="form-group">
					<input type="text" class="form-control input-lg" id="inputlg" name="username" placeholder="Isi nama lengkap anda">
				</div>
				<h6>Email</h6>
				<div class="form-group">
					<input type="password" class="form-control input-lg" id="inputlg" name="password" placeholder="Silahkan masukkan email">
				</div>
				<h6>Nomor HP</h6>
				<div class="form-group">
					<input type="password" class="form-control input-lg" id="inputlg" name="password" placeholder="Silahkan masukkan nomor hp">
				</div>
				<h6>Alamat</h6>
				<div class="form-group">
					<input type="password" class="form-control input-lg" id="inputlg" name="password" placeholder="Lengkapi alamat tinggal">
				</div>
				<h6>Kata Sandi</h6>
				<div class="form-group">
					<input type="password" class="form-control input-lg" id="inputlg" name="password" placeholder="Mohon isi kata sandi anda">
				</div>
				<h6>Ulang kata Sandi</h6>
				<div class="form-group">
					<input type="password" class="form-control input-lg" id="inputlg" name="password" placeholder="Mohon ulang kata sandi">
				</div>
				<br>
				<a href="#" style="float: left; height: 50px; width: 200px;" id='modal-launcher' class="a" data-toggle="modal" data-target="#login-modal">
					<input type="submit" class="btn btn-block btn-custom-coklat tombolregis" value="DAFTAR" />
				</a>
				<button style="float: left; background: transparent; border-color: #d69962; border-radius: 5px; height: 50px; width: 200px; margin-left: 1%;">Masuk</button>
			</form>
		</div>
	</div>
	<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 5%;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header login_modal_header">
				</div>
				<div class="modal-body login-modal">
					<div class="clearfix"></div>
					<div id='social-icons-conatainer'>
						<div class='modal-body-left'>
							<h4 class="modal-title" id="myModalLabel">Berhasil Registrasi</h4><br><br>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #d69962">OK</button>
						</div>
					</div>   
				</div>                                                                                                                
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer login_modal_footer">
			</div>
		</div>
	</div>

</body>
</html>