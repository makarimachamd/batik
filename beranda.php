<!DOCTYPE html>
<html>
<title>Batik Bayat</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="ass/css/style_tambahan.css">
<link rel="stylesheet" type="text/css" href="Boost/css/bootstrap.min.css">
<script src="Boost/js/jquery.min.js"></script>
<script type="text/javascript" src="Boost/js/bootstrap.min.js"></script>

<style>
body {
  font-family: "Lato", sans-serif;
}
</style>

<body>
  <?php 
  include 'navbar.php';
  include 'header.php';
  ?>
  <!-- Image header -->
  <div class="w3-display-container w3-container">
    <img src="gambar/batik.jpg" alt="Jeans" style="width:100%">
    <div class="w3-display-topleft w3-text-white" style="padding:24px 48px">
      <h1 class="w3-jumbo w3-hide-small">Koleksi Terbaru</h1>
      <h1 class="w3-hide-large w3-hide-medium">Koleksi Terbaru</h1>
      <h1 class="w3-hide-small">Koleksi 2018</h1>
      <p><a href="#jeans" class="w3-button w3-black w3-padding-large w3-large">BELANJA SEKARANG</a></p>
    </div>
  </div>

  <div class="w3-container w3-text-grey" id="jeans">
    <p>8 items</p>
  </div>

  <!-- Product grid -->
  <div class="w3-row w3-grayscale">
    <div class="w3-col l3 s6">
      <div class="w3-container">
        <img src="gambar/satu.jpg" style="width:100%">
        <p>Kain Batik A<br><b>Rp 900,000</b></p>
      </div>
      <div class="w3-container">
        <img src="gambar/dua.jpg" style="width:100%">
        <p>Kain Batik B<br><b>Rp 800,000</b></p>
      </div>
    </div>

    <div class="w3-col l3 s6">
      <div class="w3-container">
        <div class="w3-display-container">
          <img src="gambar/batik3.jpeg" style="width:100%">
          <span class="w3-tag w3-display-topleft">New</span>
          <div class="w3-display-middle w3-display-hover">
            <button class="w3-button w3-black">Belanja<i class="fa fa-shopping-cart"></i></button>
          </div>
        </div>
        <p>Kain Batik C<br><b>Rp 800,000</b></p>
      </div>
      <div class="w3-container">
        <img src="gambar/batik4.jpg" style="width:100%">
        <p>Kain Batik D<br><b>Rp 990,000</b></p>
      </div>
    </div>

    <div class="w3-col l3 s6">
      <div class="w3-container">
        <img src="gambar/satu.jpg" style="width:100%">
        <p>Kain Batik E<br><b>Rp 1,000,000</b></p>
      </div>
      <div class="w3-container">
        <div class="w3-display-container">
          <img src="gambar/dua.jpg" style="width:100%">
          <span class="w3-tag w3-display-topleft">Sale</span>
          <div class="w3-display-middle w3-display-hover">
            <button class="w3-button w3-black">Belanja <i class="fa fa-shopping-cart"></i></button>
          </div>
        </div>
        <p>Kain Batik F<br><b class="w3-text-red">Rp 800,000</b></p>
      </div>
    </div>

    <div class="w3-col l3 s6">
      <div class="w3-container">
        <img src="gambar/batik3.jpeg" style="width:100%">
        <p>Kain Batik G<br><b>Rp 850,000</b></p>
      </div>
      <div class="w3-container">
        <img src="gambar/batik4.jpg" style="width:100%">
        <p>Kain Batik H<br><b>Rp 1,000,000</b></p>
      </div>
    </div>
  </div>

  <?php
  include 'footer.php';
  ?>


</body>
</html>
