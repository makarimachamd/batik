<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="ass/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" type="text/css" href="Boost/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="ass/css/style_tambahan.css">
	<script src="Boost/js/jquery.min.js"></script>
	<script type="text/javascript" src="Boost/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row" style="height: 10%; background-color: black;">
			<div class="col"></div>
			<div class="col-8">
				<div class="header-search">
					<!-- <input class="input" placeholder="Search here"> -->
					<form class="mx-3 my-auto d-inline w-100">
						<div class="input-group" style="border-color: #fff; border: 5;">
							<input type="text" class="form-control border border-right-0" placeholder="Search...">
							<span class="input-group-append">
								<button class="btn btn-outline-secondary border border-left-0" type="button" style="background-color: #D69962">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</form>
				</div>
				<div class="row" style="margin-bottom: 10px">
					<div class="col"><a href="#"style="color: #fff;">Beranda</a></div>
					<div class="col"><a href="#"style="color: #fff;">Keranjang</a></div>
					<div class="col"><a href="#"style="color: #fff;">Riwayat</a></div>
					<div class="col"><a href="#"style="color: #fff;">Notifikasi</a></div>
					<div class="col"><a href="#"style="color: #fff;">Akun</a></div>
				</div>
			</div>
			<div class="col"></div>
		</div>
	</div>
</body>
</html>