<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<!-- Sidebar/menu -->
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
        	<div id="mySidenav" class="sidenav">
        		<div class="sidebar-header">
        			<h4 style="color: white">Batik Bayat</h4>
        			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()" style="color: white">&times;</a>
        		</div>
        		<ul class="list-unstyled components">

        			<li class="active">
        				<a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home
        				</a>
        				<ul class="collapse list-unstyled" id="homeSubmenu" style="margin-left: 6%;">
        					<li>
        						<a href="#">home1
        						</a>
        					</li>
        					<li>
        						<a href="#">home2
        						</a>
        					</li>
        					<li>
        						<a href="#">home3
        						</a>
        					</li>
        				</ul>
        			</li>

        			<li>
        				<a href="#">About
        				</a>
        			</li>

        			<li>
        				<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Page
        				</a>
        				<ul class="collapse list-unstyled" id="pageSubmenu" style="margin-left: 6%;">
        					<li>
        						<a href="#">page1
        						</a>
        					</li>
        					<li>
        						<a href="#">page2
        						</a>
        					</li>
        					<li>
        						<a href="#">page3
        						</a>
        					</li>
        				</ul>
        			</li>
        		</ul>

        		<a href="#footer" class="w3-bar-item w3-button w3-padding">Kontak</a> 
        		<a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding" onclick="document.getElementById('newsletter').style.display='block'">Sekilas Info</a> 
        		<a href="#footer"  class="w3-bar-item w3-button w3-padding">Lihat Lebih</a>
        	</div>
        	<!--Tombol Toggle-->
        	<div class="nav toggle">
        		<a id="menu_toggle"><i class="fa fa-bars" onclick="openNav()" style="color: white; cursor: pointer;"></i></a>
        	</div>
        	<!--/Tombol Toggle-->
        	<div class="mx-auto order-0">
        		<a class="navbar-brand" href="#">Batik Bayat</a>
        	</div>

        	<!-- <ul class="nav navbar-nav navbr-right">
        		<li class="nav-item">
        			<a class="nav-link" href="#">
        				<i class="fa fa-search"></i>
        			</a>
        		</li>
        		<li class="nav-item">
        			<a class="nav-link" href="#">
        				<i class="fa fa-shopping-cart w3-margin-right"></i>
        			</a>
        		</li>
        	</ul> -->
        </nav>
    <!--/ Sidebar/menu -->
    <script>
// Accordion 
function myAccFunc() {
  var x = document.getElementById("demoAcc");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
} else {
    x.className = x.className.replace(" w3-show", "");
}
}

// Click on the "Jeans" link on page load to open the accordion for demo purposes
document.getElementById("myBtn").click();


// Script to open and close sidebar
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>
</body>
</html>