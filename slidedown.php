<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="Boost/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="ass/css/style_tambahan.css">
	<style type="text/css">
	input {
		display: none;
	}

	label {
		cursor: pointer;
		display: inline-block;
	}

	.test {
		-webkit-transition: height .3s ease;
		height: 0;
		overflow: hidden;
	}

	input:checked + .test {
		height: 100%;
	}
</style>
</head>
<body>
	<p style="color: #777c84; font-size: 12px; font-weight: bold;">PANDUAN PEMBAYARAN</p>
	<div class="container">
		<div class="row">
			<div class="col"></div>
			<div class="col-10 outter-form">
				<div class="row">
					<div class="col-3"></div>
					<div class="col-8">
						<div class="col-2"></div>
						<div class="col-10" style="border: 1px solid #bec2c6;">
							<div class="row" style="border: 1px solid #bec2c6;">
								<div class="col-10 align-self-center">ATM MANDIRI</div>
								<div class="col-2">
									<label for="check">V</label>
								</div>
							</div>
							<input id="check" type="checkbox">

							<div class="test">
								<table>
									<tr>
										<td style="color: #a0a5af; font-size: 12px; font-weight: bold;">
											1. Masukkan kartu ATM<br>
											2. Pilih menu "Bayar/Beli"<br>
											3. Pilih menu "Lainnya" hingga menemukan menu "Multiplayment"<br>
											4. Masukkan Kode Biller Tokopedia (88708), lalu pilih benar<br>
											5. Masukkan "nomor virtual akun" Tokopedia, lalu pilih tombol benar<br>
											6. Masukkan angka "1" untuk memilih tagihan, lalu pilih tombol Ya<br>
											7. Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya<br>
											8. Simpan struk sebagai bukti pembayaran Anda
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="col-2"></div>
					</div>
					<div class="col-3"></div>
				</div>
				<div class="row">
					<div class="col-3"></div>
					<div class="col-8">
						<div class="col-2"></div>
						<div class="col-10" style="border: 1px solid #bec2c6;">
							<div class="row" style="border: 1px solid #bec2c6;">
								<div class="col-10 align-self-center">Mandiri Internet Banking / Mandiri Online</div>
								<div class="col-2">
									<label for="e-banking">V</label>
								</div>
							</div>
							<input id="e-banking" type="checkbox">

							<div class="test">
								<table>
									<tr>
										<td style="color: #a0a5af; font-size: 12px; font-weight: bold;">
											1. Masukkan kartu ATM<br>
											2. Pilih menu "Bayar/Beli"<br>
											3. Pilih menu "Lainnya" hingga menemukan menu "Multiplayment"<br>
											4. Masukkan Kode Biller Tokopedia (88708), lalu pilih benar<br>
											5. Masukkan "nomor virtual akun" Tokopedia, lalu pilih tombol benar<br>
											6. Masukkan angka "1" untuk memilih tagihan, lalu pilih tombol Ya<br>
											7. Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya<br>
											8. Simpan struk sebagai bukti pembayaran Anda
										</td>
									</tr>
									<tr>
										<td style="color: #a0a5af; font-size: 12px; font-weight: bold;">
											Jangan gunakan fitur "simpan daftar transfer" untuk pembayaran melalui internet Banking karena dapat mengganggu proses pembayaran berikutnya<br><br>
											Untuk menghapus daftar transfer tersimpan ikuti langkah berikut :<br>
											1. Login Mandiri Online<br>
											2. Pilih ke menu "Pembayaran"<br>
											3. Pilih menu daftar pembayaran<br>
											4. Pilih pada pembayaran yang tersimpan, lalu pilih menu untuk hapus
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="col-2"></div>
					</div>
					<div class="col-3"></div>
				</div>
				<div class="row">
					<div class="col">
						<center><a href="#" style="color: #4ebf74; font-size: 12px; font-weight: bold;">Detail Pembayaran</a></center>
						<button class="btn btn-custom-coklat" style="width: 100%; margin-top: 2%; margin-bottom: 2%; font-size: 100%;" type="submit">Cek Status Pembayaran</button><br>
						<img style="float: left; width: 20%" src="Gambar/visa720.png">
						<img style="float: left; width: 20%; margin-left: 6%;" src="Gambar/jne360.png">
						<img style="float: right; width: 20%;" src="Gambar/tiki240.png">
						<img style="float: right; width: 20%;  margin-right: 6%;" src="Gambar/visa720.png">
					</div>
				</div>
			</div>
			<div class="col"></div>
		</div>
	</div>
	<!--kedua-->
	<!-- <div class="container" style="border: 1px solid #bec2c6;">
		<div class="row" style="border: 1px solid #bec2c6;">
			<div class="col align-self-center">Mandiri Internet Banking / Mandiri Online</div>
			<div class="col align-self-center">
				<label for="e-banking">Click me</label>
			</div>
		</div>
		<input id="e-banking" type="checkbox">

		<div class="test">
			<table>
				<tr>
					<td style="color: #a0a5af; font-size: 12px; font-weight: bold;">
						1. Masukkan kartu ATM<br>
						2. Pilih menu "Bayar/Beli"<br>
						3. Pilih menu "Lainnya" hingga menemukan menu "Multiplayment"<br>
						4. Masukkan Kode Biller Tokopedia (88708), lalu pilih benar<br>
						5. Masukkan "nomor virtual akun" Tokopedia, lalu pilih tombol benar<br>
						6. Masukkan angka "1" untuk memilih tagihan, lalu pilih tombol Ya<br>
						7. Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya<br>
						8. Simpan struk sebagai bukti pembayaran Anda
					</td>
				</tr>
				<tr>
					<td style="color: #a0a5af; font-size: 12px; font-weight: bold;">
						Jangan gunakan fitur "simpan daftar transfer" untuk pembayaran melalui internet Banking karena dapat mengganggu proses pembayaran berikutnya<br><br>
						Untuk menghapus daftar transfer tersimpan ikuti langkah berikut :<br>
						1. Login Mandiri Online<br>
						2. Pilih ke menu "Pembayaran"<br>
						3. Pilih menu daftar pembayaran<br>
						4. Pilih pada pembayaran yang tersimpan, lalu pilih menu untuk hapus
					</td>
				</tr>
			</table>
		</div>
	</div> -->
</body>
</html>