<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="Boost/js/jquery.min.js"></script>
	<script type="text/javascript" src="Boost/js/bootstrap.min.js"></script>

	<style>
	body {
		font-family: "Lato", sans-serif;
	}
</style>
	<link rel="stylesheet" type="text/css" href="Boost/css/bootstrap.min.css">
	<link rel="stylesheet" href="ass/css/style_tambahan.css" >

</head>
<body>

	<?php
	include 'navbar.php';
	include 'header.php';
	?>

	<br>
	<div class="row">
		<div class="col"></div>
		<div class="col-8 outter-form" style="padding: 1%">
			<p style="color: #52565b; font-size: 18px; font-weight: bold;">Menunggu Pembayaran (1)</p>
			
			<div class="col-12 outter-form" style="border: 1px solid #bec2c6;">
				<div class="row">
					<div class="col-1" style="margin-top: 10px;">
						<img style="width: 80%;" src="Gambar/shopping-bag.svg">
					</div>

					<div class="col-8" style="margin: 0%; padding: 0%">
						<div style="color: #52565b; font-size: 16px; font-weight: bold; margin-bottom: 0px;">Belanja</div>
						<div class="row" style="padding: 0%;">
							<div class="col-1" style="color: #afb2b5; font-size: 14px; font-weight: bold; margin-bottom: 0px;">
								Total
							</div>
							<div class="col-md-auto" style="color: #d69962; font-size: 14px; font-weight: bold; margin-right: 0px;">
								Rp. 14.750.098
							</div>
							<div align="center">|</div>
							<div class="col-md-auto" style="color: #afb2b5; font-size: 14px; font-weight: bold;">
								Tanggal Pembelian
							</div>
							<div style="color: #52565b; font-size: 14px; font-weight: bold;">
								22 Dec 2018
							</div>
						</div>
						<div class="col-18" style="background: lightgrey; padding: 1%; margin-top: 5px;">
							Bayar Sebelum 24 Dec 2018, 03:59 WIB
						</div>
						<table style="color: #52565b; font-size: 14px; font-weight: bold; margin-top: 5px; padding: 2%; line-height: 200%;">
							
							<tr>
								<td style="color: #afb2b5; ">Metode Pembayaran</td>	
								<td>:</td>
								<td>Mandiri Virtual Akunmm mmmmmmm mddd dddd dddddmm</td>
							</tr>
							
							<tr>
								<td style="color: #afb2b5">Nomor Virtual Akun</td>
								<td>:</td>
								<td>8886738848899439</td>	
							</tr>
						</table>
					</div>

					<div class="col-md-3">
						<div class="row justify-content-center">
							<div class="row align-items-start">
								<a style="color: #4ebf74; font-size: 12px; font-weight: bold; float: right; margin-top: 10px; margin-right: 0px;" href="#"  id='modal-launcher' class="a" data-toggle="modal" data-target="#login-modal">Batalkan Transaksi</a>
							</div>
							<div align="center" style="margin-top: 15%;">
								<img style="width: 30%;" src="Gambar/visa.png">
							</div>
							<div class="row align-items-end" style="margin-top: 15%;">
								Cara Pembayaran
							</div>
						</div>
						
					</div>

				</div>
			</div><br>

		</div>
		<div class="col"></div>
	</div>

	<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 5%;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body login-modal">
					<div class="clearfix"></div>
					<div id='social-icons-conatainer'>
						<div class='modal-body-left'>
							<h4 class="modal-title" id="myModalLabel">Apakah anda yakin akan membatalkan transaksi?</h4><br><br>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #d69962">Batal</button>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #d69962">Ya&nbsp;&nbsp;</button>
						</div>
					</div>   
				</div>                                                                                                                
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer login_modal_footer">
			</div>
		</div>
	</div>


</body>
</html>